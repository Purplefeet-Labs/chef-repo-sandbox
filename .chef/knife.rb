# See https://docs.getchef.com/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "purplefeetguy"
client_key               "#{current_dir}/purplefeetguy.pem"
validation_client_name   "pflabs-sb-validator"
validation_key           "#{current_dir}/pflabs-sb-validator.pem"
chef_server_url          "https://server-centos65/organizations/pflabs-sb"
cookbook_path            ["#{current_dir}/../cookbooks"]
